Roger Parks is an engineer with interests in mechatronics systems, embedded systems and programming.

<table>
  <tr>
    <th>Website</th>
    <td>https://rogerparks.com/</td>
  </tr>
  <tr>
    <th>YouTube</th>
    <td>https://www.youtube.com/c/RogerParks0</td>
  </tr>
</table>
